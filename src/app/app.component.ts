import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  
  constructor(){
  }

  ngOnInit(): void {
    document.getElementById('d1').style.backgroundColor = 'rgb(44,136,217)';
    document.getElementById('d2').style.backgroundColor = 'rgb(211,69,91)';
    document.getElementById('d3').style.backgroundColor = 'rgb(137,122,95)';
    document.getElementById('d4').style.backgroundColor = 'rgb(172,99,99)';
    document.getElementById('d5').style.backgroundColor = 'rgb(115,15,195)';
    document.getElementById('d6').style.backgroundColor = 'rgb(26,174,159)';
    document.getElementById('d7').style.backgroundColor = 'rgb(32,120,104)';
    document.getElementById('d8').style.backgroundColor = 'rgb(101,88,245)';
    document.getElementById('d9').style.backgroundColor = 'rgb(75,92,107)';
    this.onResize();
  }

  onResize(){ 
    let div7 = document.getElementById('div-seven');
    let macroDiv4 = document.getElementById('macro-div-4');
    let div2 = document.getElementById('div-two');
    let macroDiv3 = document.getElementById('macro-div-3');
    let fakeDiv = document.getElementById('fake-mobile');
    let lastDiv = document.getElementById('last-div');
    let macroDiv1 = document.getElementById('macro-div-1');
    let macroDiv2 = document.getElementById('macro-div-2');
    if(window.innerWidth < 601){
      fakeDiv.appendChild(div7);
      fakeDiv.className='spacer';
      div2.parentNode.insertBefore(macroDiv3, div2);
      div2.classList.add('spacer-up');
      macroDiv1.classList.remove('spacer-right');
      macroDiv2.classList.remove('spacer-left');
    }
    else{
      lastDiv.appendChild(div7);
      fakeDiv.className = '';
      div7.parentNode.insertBefore(div7, macroDiv4);
      div2.parentNode.insertBefore(div2, macroDiv3);
      div2.classList.remove('spacer-up');     
      macroDiv1.classList.add('spacer-right');
      macroDiv2.classList.add('spacer-left');
    }
  }

  changeDivColor(){
    document.getElementById('d1').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d2').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d3').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d4').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d5').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d6').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d7').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d8').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
    document.getElementById('d9').style.backgroundColor = "rgb("+Math.round(Math.random()*255)+","+Math.round(Math.random()*255)+","+
    Math.round(Math.random()*255)+")";
  }

  

}
